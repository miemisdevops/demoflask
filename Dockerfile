FROM python:latest

RUN pip install flask

RUN pip install Flask-RESTful

WORKDIR /app

COPY main.py ./

ENTRYPOINT ["python3"]

CMD ["main.py"]
